package com.maxbill.fxui.util;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 主窗对象
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public class DesktopObjects {

    /**
     * 窗口坐标及高宽
     */
    public static double WIN_X = 0;
    public static double WIN_Y = 0;
    public static double WIN_W = 0;
    public static double WIN_H = 0;
    public static double MIN_W = 1000;
    public static double MIN_H = 600;

    /**
     * 窗口偏移量
     */
    public static double X_OFFSET = 0;
    public static double Y_OFFSET = 0;

    /**
     * 窗口最大最小以及重置宽度
     */
    public static double MIN_SCENE = 30;
    public static double RESIZE_WIDTH = 5;


    /**
     * 是否处于右边界调整窗口状态
     */
    public static boolean IS_RIGHT;

    /**
     * 是否处于下边界调整窗口状态
     */
    public static boolean IS_BOTTOM;

    /**
     * 是否处于右下角调整窗口状态
     */
    public static boolean IS_BOTTOM_RIGHT;

    /**
     * 是否处于最大化调整窗口状态
     */
    public static boolean IS_MAX = false;

    /**
     * 是否处于全屏窗口状态
     */
    public static boolean IS_FULL = false;

    /**
     * 当前键盘按键编码
     */
    public static String KEY_CODE = "";

    /**
     * 根窗口对象
     */
    public static Stage rootWin;

    /**
     * 主窗口对象
     */
    public static BorderPane mainWin;

    /**
     * 顶部窗口对象
     */
    public static BorderPane headWin;

    /**
     * 应用上下文
     */
    public static ConfigurableApplicationContext context;

}
