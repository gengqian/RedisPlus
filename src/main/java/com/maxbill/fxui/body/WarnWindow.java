package com.maxbill.fxui.body;

import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static com.maxbill.fxui.body.BodyWindow.getTabPane;
import static com.maxbill.fxui.util.CommonConstant.TEXT_TAB_WARN;

/**
 * 监控预警窗口
 *
 * @author MaxBill
 * @date 2019/07/10
 */
public class WarnWindow {

    private static Tab warnTab;


    public static void initWarnTab() {
        if (null == warnTab) {
            warnTab = new Tab(TEXT_TAB_WARN);
            getTabPane().getTabs().add(warnTab);
        }
        VBox warnPane = new VBox();
        HBox baseWarn = new HBox();
        baseWarn.setPrefHeight(140);
        VBox uptimeBox = new VBox();
        uptimeBox.setPrefHeight(100);
        uptimeBox.setMaxWidth(200);

        VBox clientBox = new VBox();
        clientBox.setPrefHeight(100);
        clientBox.setMaxWidth(200);

        VBox memoryBox = new VBox();
        memoryBox.setPrefHeight(100);
        memoryBox.setMaxWidth(200);

        VBox bufferBox = new VBox();
        bufferBox.setPrefHeight(100);
        bufferBox.setMaxWidth(200);

        VBox binaryBox = new VBox();
        binaryBox.setPrefHeight(100);
        binaryBox.setMaxWidth(200);

        VBox keynumBox = new VBox();
        keynumBox.setPrefHeight(100);
        keynumBox.setMaxWidth(200);

        baseWarn.getChildren().add(uptimeBox);
        baseWarn.getChildren().add(clientBox);
        baseWarn.getChildren().add(memoryBox);
        baseWarn.getChildren().add(bufferBox);
        baseWarn.getChildren().add(binaryBox);
        baseWarn.getChildren().add(keynumBox);

        warnPane.getChildren().add(baseWarn);

        warnTab.setContent(warnPane);
        getTabPane().getSelectionModel().select(warnTab);
        warnTab.setOnClosed(event -> warnTab = null);
    }

}
